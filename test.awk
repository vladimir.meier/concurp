{
    if($3 == "shares")  {
        print $0
        roommates[$2] = $NF
    }
}
END{
    valid = 0
    for (i in roommates)    {
        if(roommates[roommates[i]] !=i )    {
            valid = 1
            f = roommates[i]
            ff = roommates[f]
            print "[!] Error, Roommate #" i " shares a room with #" f ", who already shares a room with #" ff
            break
        }
    }

    if(valid == 0)
        print "[*] Test passed"
    else
        print "[!] Test failed"
}
