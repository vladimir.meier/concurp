#!/bin/bash

#simple test program for Roommates simulation. For now it only validates the result, it doesn't check
#the quality of the solution

#PRE : only final results and prefs are printed to stdout. Use System.err.printf for garbage/debug
#copyright : who cares 

#variables
x=""
p=1
i=0

#constants. config goes here
PROJECT_FILES=$(ls *.jr)
DEBUG_FILE="test.dbg"
PROG_FILE="test.awk"
MAX_ROOMS=10
KEYWORD="passed" #defined in $PROG_FILE
CMD="jrgo$x $p 2>$DEBUG_FILE | awk -f $PROG_FILE"
ITERATIONS=20

#executes jrgo once to compile everything
function init()
{
    
    echo "-----------------------------------------------------------------------"
    echo "[*] Tests init"
    echo "[*] command : $CMD"
    touch $DEBUG_FILE
    res=$(bash -c "$CMD")
    x="x" #disables compilation for next tests
}

#test parameters 1..$MAX_ROOMS only once
function basic_tests()
{
    p=1

    res=$(bash -c "$CMD")

    echo "[*] Test parameters 1..$MAX_ROOMS only once"

    while [[ $res == *$KEYWORD* && $p -le $MAX_ROOMS ]];do
        echo -e "\t * basic test with parameter $p : $CMD" 
        res=$(bash -c "$CMD")
        p=$((p+1))
        CMD="jrgo$x $p 2>$DEBUG_FILE | awk -f $PROG_FILE"
    done

    if [[ $res == *$KEYWORD* ]];then
        echo "[*] Basic test passed"
    else
        echo "[!] FAIL. Printing debug_file to see what happened"
        echo "$res"
        cat $DEBUG_FILE
    fi 
}

#test the same parameter until #ITERATIONS to find a bug
function intensive_test()
{
    p=7 #modifiy this parameter according to your project
    i=0
    CMD="jrgo$x $p 2>$DEBUG_FILE | awk -f $PROG_FILE"
    echo "$CMD"
    res=$(bash -c "$CMD")

    echo "[*] Test parameters $p, $ITERATIONS times to find a bug"

    while [[ $res == *$KEYWORD* && $i -le $ITERATIONS ]];do
        echo -e "\t * Iteration #$i" 
        res=$(bash -c "$CMD")
        i=$((i+1))
        echo "$res"
    done

    if [[ $res == *$KEYWORD* ]];then
        echo "[*] Test passed"
    else
        echo "[!] FAIL. Printing debug_file to see what happened"
        echo "$res"
        cat $DEBUG_FILE
    fi 
}

#4 : tab character
function check_tab()
{
    echo -e "\t * Checking #4"
    tabs=$(grep -c '	' $1)
    if [[ $tabs -ne 0 ]];then
        echo "[!][#4] $tabs tab characters found"
    else
        echo -e "\t * $1 is fine"
    fi
}

#4 : no less than 3 spaces, no more than 8 spaces per indent. Doesn't work, todo
function check_indent()
{
    echo "Checking #4"
    cat "$1" | awk -F '[[:whitespace:]]' '
    {
        count=0
        for(i = 1 ; i < NF ; i++)
        {
            if(match(/[^[:space:]]/)
                break
                count++
            }

            print count " " $0
        }
    }'    
}

#7 : no lines shall be longer than 120 characters
function check_length()
{
    echo -e "\t * Checking #7"
    bad_lines_cmd="awk 'length(\$0) > 120' $1"
    bad_lines=$(bash -c "$bad_lines_cmd")
    cnt=$(wc -L $1 | awk '{print $1}')
    if [[ "$cnt" -gt 120 ]];then
        echo "[!][#7] These lines are too long :"
        echo "$bad_lines"
    else
        echo -e "\t * $1 is fine"
    fi
}

#commandments compliance
function check()
{
    while test -n "$1"
    do
        echo "Checking file $1 for commandments errors"
        check_tab $1
        check_length $1
        shift
    done
}

check $PROJECT_FILES
init
basic_tests
intensive_test
